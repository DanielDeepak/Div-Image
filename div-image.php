
<!-- 
Change image on hover
This Changes image on hover effect on div using js
@Author Daniel Deepak
-->


<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" type="text/css" href="mystyle.css">
	<title>div-image</title>
	<script>
	/**
	 * to change image source for the onmouseover event
	 * @author [Daniel Deepak]
	 * @param  id [id of the hover element]
	 */
		function change_image(id){
			if(id=="third-div"){

				document.getElementById("image-select").src='image3.jpg'; //change the image source of img element
				console.log("div3");

			} else if(id=="second-div"){
				document.getElementById("image-select").src='image2.jpg'; //change the image source of img element
				console.log("div2");
			} else {
				document.getElementById("image-select").src='image1.jpg'; //change the image source of img element
				console.log("div1");
			} 

		}

		/**
		 * to change to the default first image when hover out from second and third block or div
		 * @author [Daniel Deepak]
		 * @param  id [id of the hover element]
		 */

		function default_image(id){
			if(id=="second-div" || id=="third-div"){
				document.getElementById("image-select").src='image1.jpg';

			} 
		}
 
	</script>
</head>
<body> 
	<!-- first block -->
	<div id="first-div" onmouseover="change_image(this.id);" >
		<h2 class="div-heading">first image</h2>
	</div>

	<!-- second block -->
	<div id="second-div" onmouseover="change_image(this.id);"  onmouseout="default_image(this.id);">
		<h2 class="div-heading">second image</h2>
	</div>

	<!-- third block -->
	<div id="third-div" onmouseover="change_image(this.id);" onmouseout="default_image(this.id);">
		<h2 class="div-heading">third image</h2>
	</div>


	<div id="image-container">
		<img id="image-select" src="image1.jpg" width="600px" height="300px" />
	</div>

	
</body>
</html> 